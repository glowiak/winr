
all: winr jar

winr:
	javac com/glowiak/winr/WinR.java

jar:
	jar cvfe winr.jar com.glowiak.winr.WinR com/glowiak/winr/*.class

clean:
	find . -name '*.class' -delete
	rm -f winr.jar
