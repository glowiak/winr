package com.glowiak.winr;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WinR
{
    public static final int WIDTH = 500;
    public static final int HEIGHT = 250;
    
    public static WinR me;
    
    public JFrame w;
    
    private JLabel l_info;
    private JLabel l_info1;
    private JLabel l_open;
    private JButton b_ok;
    private JButton b_cancel;
    private JButton b_browse;
    public JTextField tf_open;
    
    public static void main(String[] args)
    {
        me = new WinR();
    }
    
    public WinR()
    {
        w = new JFrame("Run");
        w.setSize(WIDTH + 25, HEIGHT + 25);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setVisible(true);
        w.setLayout(null);
        
        l_info = new JLabel("Type the name of a program, folder document or Internet");
        l_info.setBounds(50, 15, WIDTH - 50 - 10, 25);
        
        l_info1 = new JLabel("resource, and Linux will open it for you.");
        l_info1.setBounds(50, 35, WIDTH - 50 - 10, 25);
        
        l_open = new JLabel("Open:");
        l_open.setBounds(0, 75, 100, 25);
        
        tf_open = new JTextField();
        tf_open.setBounds(50, 75, WIDTH - 70, 25);
        
        b_browse = new JButton("Browse...");
        b_browse.setBounds(WIDTH - 145, HEIGHT - 55, 120, 25);
        
        b_browse.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int jfr = jfc.showOpenDialog(null);
                if (jfr == JFileChooser.APPROVE_OPTION)
                {
                    tf_open.setText(jfc.getSelectedFile().getPath());
                }
            }
        });
        
        b_cancel = new JButton("Cancel");
        b_cancel.setBounds(WIDTH - 145 - 125, HEIGHT - 55, 120, 25);
        
        b_cancel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });
        
        b_ok = new JButton("OK");
        b_ok.setBounds(WIDTH - 145 - 125*2, HEIGHT - 55, 120, 25);
        
        b_ok.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                int r = 0;
                w.setVisible(false);
                try {
                    Process p = Runtime.getRuntime().exec(tf_open.getText());
                    r = p.waitFor();
                } catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                
                if (r != 0)
                {
                    w.setVisible(true);
                    JOptionPane.showMessageDialog(null, "ERROR: The proces has exited with a non-zero error code (" + r + ")!");
                } else
                {
                    System.exit(0);
                }
            }
        });
        
        w.add(l_info);
        w.add(l_info1);
        w.add(l_open);
        w.add(tf_open);
        w.add(b_browse);
        w.add(b_cancel);
        w.add(b_ok);
        
        w.getRootPane().setDefaultButton(b_ok);
        
        while (true)
        {
            w.setSize(WIDTH, HEIGHT);
        }
    }
}
